package BankAccount;

import java.util.InputMismatchException;
import java.util.Scanner;

import BankAccount.Bank;

public class FinalCode {
	public static void BankAccount() {
		BankDeposit D1 = new BankDeposit();
		BankWithdrawal W1 = new BankWithdrawal();
		int select = 0;
		int accNum = 0;
		String op = "";
		String name = "";
		Scanner scan = new Scanner(System.in);
		System.out.println("\n\tWELCOME TO THE CEYLON BANKING SYSTEM\n\t____________________________________\n\n");
		do {
			System.out.print("ENTER YOUR ACCOUNT NAME : ");
			name = scan.next();
			if (name.matches("[\\-]?[0-9]+")) {
				System.err.println("\tNAME SHOULD BE LETTERS!!!");
			}
		} while (name.matches("[\\-]?[0-9]+"));

		boolean validInput = false;
		while (!validInput) {
		System.out.print("ENTER YOUR ACCOUNT NUMBER : ");
		try {
			accNum = scan.nextInt();
			validInput = true;
		} catch (InputMismatchException e) {
			System.err.println("\tPLEASE ENTER AN NUMBER!!!");
			scan.next();
		}
	}

		System.out.print("*****************************************\n" + "      NAME >>> " + name.toUpperCase() + "\n"
				+ "      ACCOUNT NUMBER >>> " + accNum + "\n" + "*****************************************\n\n");

		System.out.println("NOW UPDATING TIME : ");
		Bank.Time();

		System.out.print("\nENTER YOUR CURRENT BALANCE >>> ");
		int money = scan.nextInt();
		Bank.accBal = money;

		do {
			boolean validInput1 = false;
			while (!validInput1) {
				System.out.print("________________________________\n" + "|                               |\n"
						+ "|        WHAT DO YOU WANT       |\n" + "|  DEPOSIT >>> press 1          |\n"
						+ "|  WITHDRAWAL >>> press 2       |\n" + "|  CHECK BALANCE >>> press 3    |\n"
						+ "|  EXIT >>> press 4             |\n" + "|_______________________________|\n");
				System.out.print(">>>Type Here : ");
				try {
					select = scan.nextInt();
					validInput1 = true;
				} catch (InputMismatchException e) {
					System.err.println("\tPLEASE ENTER AN INTEGER!!!");
					scan.next();
				}
			}

			if (select != 1 && select != 2 && select != 3 && select != 4) {
				System.err.println("\t!!! INVALID NUMBER !!!");
			}

			if (select == 1) {
				D1.dep();
			}

			if (select == 2) {
				W1.wid();
			}

			if (select == 3) {
				System.out.println("\n\t" + name.toUpperCase() + " YOUR CLOSING BALANCE IS Rs. " + Bank.accBal);
				String output = (50000 <= Bank.accBal)
						? "\t*** " + name.toUpperCase() + " it is time to invest some money *** "
						: (15000 < Bank.accBal) ? "\t*** " + name.toUpperCase() + " you should consider a CD ***"
								: (1000 < Bank.accBal) ? "\t*** " + name.toUpperCase() + " keep up the good work ***"
										: "\t*** " + name.toUpperCase() + " your balance is getting low ***";
				System.out.println(output);
			}

			if (select == 4) {
				do {
					System.out.println("_____________________________________________\n"
							+ "|             ARE YOU WANT TO EXIT           |\n"
							+ "|               YES >>> press 'y'            |\n"
							+ "|               NO >>> press 'n'             |\n"
							+ "|____________________________________________|");
					System.out.print(">>>Type Here : ");
					op = scan.next();
					if (!op.equals("y") && !op.equals("n")) {
						System.err.println("\t!!! INVALID OPTION !!!");
					}
				} while (!op.equals("y") && !op.equals("n"));

				if (op.equals("y")) {
					break;
				}
			}

		} while (true);

	}
}
