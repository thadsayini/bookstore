
	package BankAccount;

	import java.util.Scanner;

	public class BankWithdrawal {
		Scanner scan = new Scanner(System.in);
		Double wid, widAmt;

		public void wid() {
			do {
				System.out.println(
						"\nHOW MANY TIMES ARE YOU PLANNING WITHDRAWALS (note: only 5 times you can withdrawal at one time) : ");
				wid = scan.nextDouble();
				if (wid > 5 || wid < 0) {
					System.err.println("\t\n!!! Invalid number of withdrawals, please re-enter !!!");
				}
			} while (wid > 5 || wid < 0);

			for (int i = 1; i <= wid; i++) {
				do {
					System.out.print("-->ENTER THE AMOUNT OF WITHDRAWAL NUMBER " + i + " : ");
					widAmt = scan.nextDouble();
					if (widAmt < 0) {
						System.err.println("\t\n!!! Deposit amount must be greater than zero, please re-enter !!!");
					}
					if (widAmt > Bank.accBal) {
						System.err.println("\t\n!!! Withdrawal amount exceeds current balance, please re-enter !!!");
					}
				} while (widAmt < 0 || widAmt > Bank.accBal);
				Bank.widthdraw(widAmt);
			}
			System.out.println("\n\tBALANCE IS : " + Bank.accBal);
		}
	}


