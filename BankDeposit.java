package BankAccount;

import java.util.Scanner;

public class BankDeposit {
	Scanner scan = new Scanner(System.in);
	Double dep, depAmt;

	public void dep() {
		do {
			System.out.println(
					"\nHOW MANY TIMES ARE YOU PLANNING DEPOSIT (note: only 5 times you can deposit at one time) : ");
			dep = scan.nextDouble();
			if (dep > 5 || dep < 0) {
				System.err.println("\t\n!!!!! Invalid number of deposits, please re-enter !!!!!");
			}
		} while (dep > 5 || dep < 0);

		for (int i = 1; i <= dep; i++) {
			do {
				System.out.print("-->ENTER THE AMOUNT OF DEPOSIT NUMBER " + i + " : ");
				depAmt = scan.nextDouble();
				if (depAmt < 0) {
					System.err.println("\t\n!!! Deposit amount must be greater than zero, please re-enter !!!");
				}
			} while (depAmt < 0);
			Bank.deposit(depAmt);
		}
		System.out.println("\n\tBALANCE IS : " + Bank.accBal);
	}

}

